//
//  ViewController.swift
//  KPIFilter
//
//  Created by Prakash Maripalli on 7/30/20.
//  Copyright © 2020 Prakash Maripalli. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func didTapFilter(_ sender: Any) {
        
        let viewModel = FilterBaseVM()
        let vc = FilterLandingViewController(viewModel: viewModel)
        let navVc = UINavigationController(rootViewController: vc)
        navVc.modalPresentationStyle = .fullScreen
        present(navVc, animated: true)
    }
    
}

