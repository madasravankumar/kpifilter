//
//  TerritoryView.swift
//  KPIFilter
//
//  Created by Sravan kumar on 8/2/20.
//  Copyright © 2020 Prakash Maripalli. All rights reserved.
//

import UIKit

protocol TerritoryViewDelegate: NSObjectProtocol {
    func didSelect(level: Int, territory: String?)
    func didTapOnRadioBtn()
}

class TerritoryView: KPIBaseView {
    weak var delegate: TerritoryViewDelegate?
    
    var viewModel: TerritoryVM!
    var selectedIndex: [Int] = [Int]()
    
    init(viewModel: TerritoryVM) {
        super.init(frame: .zero)
        self.viewModel = viewModel
        configure()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func configure() {
        super.configure()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(TerritoryTableViewCell.self, forCellReuseIdentifier: TerritoryTableViewCell.reuseIdentifier)
        tableView.register(UINib(nibName: "TerritoryTableViewCell", bundle: nil), forCellReuseIdentifier: TerritoryTableViewCell.reuseIdentifier)
    }
    
    @objc func radioBtnTapped(_ sender: UITapGestureRecognizer) {
        guard let view = sender.view else { return }
        let indexPath = IndexPath(row: view.tag, section: 0)
        
        if selectedIndex.contains(indexPath.row) {
            let index = selectedIndex.firstIndex(of: indexPath.row)
            selectedIndex.remove(at: index ?? 0)
        } else {
            selectedIndex.append(indexPath.row)
        }
        tableView.reloadRows(at: [indexPath], with: .automatic)

        //let model = viewModel.results[indexPath.row]
        //NotificationCenter.default.post(name: .NSNotificationWillSelectTerritory, object: nil, userInfo: ["level": viewModel.selectedLevel, "territory": model.name ?? ""])
        //delegate?.didTapOnRadioBtn()
    }
}

extension TerritoryView: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRows()
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let tableViewCell = cell as? TerritoryTableViewCell else { return }
        if indexPath.row == 0 {
            tableViewCell.roundCorners(corners: [.topLeft, .topRight], radius: 15)
        }else if viewModel.numberOfRows() == indexPath.row + 1 {
            tableViewCell.roundCorners(corners: [.bottomLeft, .bottomRight], radius: 15)
        } else {
            tableViewCell.roundCorners(corners: [.topRight, .topRight, .bottomLeft, .bottomRight], radius: 0)
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: TerritoryTableViewCell.reuseIdentifier) as? TerritoryTableViewCell

        if viewModel.selectedLevel == 0 {
            cell?.updateLevelData(title: viewModel.dataSource(at: indexPath))
        } else {
            let model = viewModel.results[indexPath.row]
            cell?.updateInfo(title: model.name, shouldSelect: selectedIndex.contains(indexPath.row))
            cell?.radioImgView?.isUserInteractionEnabled = true
            cell?.radioImgView?.tag = indexPath.row
            
            let tap = UITapGestureRecognizer(target: self, action: #selector(radioBtnTapped(_:)))
            tap.numberOfTapsRequired = 1
            cell?.radioImgView?.addGestureRecognizer(tap)
        }
        return cell ?? UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = KPITableHeaderView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.width, height: 40))
        headerView.configure(title: "TERRITORY LEVELS")
        return headerView
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if viewModel.selectedLevel == 0 {
            delegate?.didSelect(level: indexPath.row + 1, territory: nil)
        } else {
            
            if indexPath.row == 0 {
                return
            }
            
            let model = viewModel.results[indexPath.row]
            delegate?.didSelect(level: viewModel.selectedLevel + 1, territory: model.name)
        }
    }
}
