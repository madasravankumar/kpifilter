//
//  FilteLandingCell.swift
//  KPIFilter
//
//  Created by Prakash Maripalli on 7/30/20.
//  Copyright © 2020 Prakash Maripalli. All rights reserved.
//

import UIKit

class FilteLandingCell: UITableViewCell {
    static let reuseIdentifier = "landingCell"
    
    @IBOutlet weak var root: UIView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var subTitleLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        backgroundColor = .clear
        root.backgroundColor = UIColor.cellBackground
        selectionStyle = .none
        root.layer.cornerRadius = 10
        root.layer.masksToBounds = true
    }
   
//    override func layoutSubviews() {
//        self.bounds = CGRect(x: self.bounds.origin.x, y: self.bounds.origin.y, width: self.bounds.size.width - 30, height: self.bounds.size.height)
//        super.layoutSubviews()
//    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
