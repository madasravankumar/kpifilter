//
//  AccountsTableViewCell.swift
//  KPIFilter
//
//  Created by Sravan kumar on 8/29/20.
//  Copyright © 2020 Prakash Maripalli. All rights reserved.
//

import UIKit

class AccountsTableViewCell: UITableViewCell {
    static let reuseIdentifier = "AccountsCell"
    
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var checkmarkImgView: UIImageView!
    @IBOutlet weak var rootView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        backgroundColor = .clear
        rootView.backgroundColor = UIColor.cellBackground

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func updateInfo(title: String?, shouldSelect: Bool) {
        titleLbl.text = title
        checkmarkImgView.image = shouldSelect ? UIImage(named: "checkmark") : nil
    }
}

extension AccountsTableViewCell {
    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.rootView.layer.mask = mask
        self.rootView.layer.masksToBounds = true
    }
}
