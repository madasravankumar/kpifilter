//
//  TerritoryTableViewCell.swift
//  KPIFilter
//
//  Created by Sravan kumar on 8/30/20.
//  Copyright © 2020 Prakash Maripalli. All rights reserved.
//

import UIKit

class TerritoryTableViewCell: UITableViewCell {
    static let reuseIdentifier = "TerritoryCell"
    
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var radioImgView: UIImageView!
    @IBOutlet weak var rootView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        selectionStyle = .none
        backgroundColor = .clear
        rootView.backgroundColor = UIColor.cellBackground
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func updateInfo(title: String?, shouldSelect: Bool) {
        titleLbl.text = title
        let image = shouldSelect ? "check" : "uncheck"
        radioImgView?.image = UIImage(named: image)
    }
    
    func updateLevelData(title: String?) {
        titleLbl.text = title
        if radioImgView != nil {
            radioImgView.removeFromSuperview()
        }
    }
}

extension TerritoryTableViewCell {
    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.rootView.layer.mask = mask
        self.rootView.layer.masksToBounds = true
    }
}
