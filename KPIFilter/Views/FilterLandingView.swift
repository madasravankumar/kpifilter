//
//  FilterLandingView.swift
//  KPIFilter
//
//  Created by Prakash Maripalli on 7/30/20.
//  Copyright © 2020 Prakash Maripalli. All rights reserved.
//

import UIKit

protocol FilterLandingProtocol: NSObjectProtocol {
    func didTap(at index: Int)
    func didTapTerritoryLevel(level: Int)
}

class FilterLandingView: KPIBaseView {
    weak var delegate: FilterLandingProtocol?
    
    var viewModel: FilterBaseVM!
    
    init(viewModel: FilterBaseVM) {
        super.init(frame: .zero)
        self.viewModel = viewModel
        configure()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func configure() {
        super.configure()
        backgroundColor = .white
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(UINib(nibName: "FilteLandingCell", bundle: nil), forCellReuseIdentifier: FilteLandingCell.reuseIdentifier)
    }
    
    func updateFiscalData() {
        guard let cell = tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as? FilteLandingCell else { return }
        cell.subTitleLbl.text = viewModel.fiscalData?.name
    }
    
    private func createTerritoryLevels() -> UIStackView {
        let stackView = UIStackView(frame: CGRect(x: 20, y: 0, width: 250, height: 25))
        stackView.alignment = .leading
        stackView.distribution = .fillProportionally
        stackView.axis = .horizontal
        
        guard let level = viewModel.territoryLevel else { return UIStackView() }
        for i in 0 ..< level {
            let lbl = UILabel()
            lbl.isUserInteractionEnabled = true
            lbl.text = "All >"
            lbl.font = UIFont.systemFont(ofSize: 13)
            lbl.textColor = .lightGray
            lbl.tag = i + 1
            let tap = UITapGestureRecognizer(target: self, action: #selector(levelTapped(sender:)))
            tap.numberOfTapsRequired = 1
            lbl.addGestureRecognizer(tap)
            stackView.addArrangedSubview(lbl)
        }
        return stackView
    }
    
    @objc func levelTapped(sender: UITapGestureRecognizer) {
        guard let view = sender.view else { return }
        delegate?.didTapTerritoryLevel(level: view.tag)
        print(view.tag)
    }
    
    func updateTerritoryLevel() {
        tableView.reloadSections(IndexSet(arrayLiteral: 1), with: .automatic)
    }
}

extension FilterLandingView: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: FilteLandingCell.reuseIdentifier) as! FilteLandingCell
        switch indexPath.section {
        case 0:
            cell.titleLbl?.text = "Time"
        case 1:
            cell.titleLbl?.text = "Territorry Levels"
            cell.subTitleLbl.text = viewModel.territory ?? "All"
        case 2:
            cell.titleLbl?.text = "Accounts"
        case 3:
            cell.titleLbl?.text = "Stores"
        default:
            break
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let title = viewModel.fetchHeaderTitle(at: section)
        let view = KPITableHeaderView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.width, height: 30))
        view.configure(title: title)
        return view
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: self.tableView.frame.width, height: 25))
        if section == 1 {
            let stackView = createTerritoryLevels()
            view.addSubview(stackView)
        }
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 25
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.didTap(at: indexPath.section)
    }
}
