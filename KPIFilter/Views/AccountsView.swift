//
//  AccountsView.swift
//  KPIFilter
//
//  Created by Sravan kumar on 8/1/20.
//  Copyright © 2020 Prakash Maripalli. All rights reserved.
//

import UIKit

class AccountsView: KPIBaseView {
    
    var viewModel: AccountsVM!
    var selectedRows: [Int] = [Int]()
    var headerView: KPITableHeaderView?
    
    init(viewModel: AccountsVM) {
        super.init(frame: .zero)
        self.viewModel = viewModel
        configure()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func configure() {
        super.configure()
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(AccountsTableViewCell.self, forCellReuseIdentifier: AccountsTableViewCell.reuseIdentifier)
        tableView.register(UINib(nibName: "AccountsTableViewCell", bundle: nil), forCellReuseIdentifier: AccountsTableViewCell.reuseIdentifier)
    }
    
    private func updateSelectedCount() {
        if viewModel.accountType == .accounts {
            headerView?.titleLbl.text = "ACCOUNTS (MAX \(viewModel.selectedAccounts.count)/5)"
        }else {
            headerView?.titleLbl.text = "STORES (MAX \(viewModel.selectedStores.count)/5"
        }
    }
}

extension AccountsView: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.fetchNumberOfRows()
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let tableViewCell = cell as? AccountsTableViewCell else { return }
        if indexPath.row == 0 {
            tableViewCell.roundCorners(corners: [.topLeft, .topRight], radius: 15)
        }else if viewModel.fetchNumberOfRows() == indexPath.row + 1 {
            tableViewCell.roundCorners(corners: [.bottomLeft, .bottomRight], radius: 15)
        } else {
            tableViewCell.roundCorners(corners: [.topRight, .topRight, .bottomLeft, .bottomRight], radius: 0)
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: AccountsTableViewCell.reuseIdentifier) as? AccountsTableViewCell
        cell?.selectionStyle = .none
        let text = viewModel.fetchDataSource(at: indexPath)
        let contains = viewModel.checkRecordShouldSelect(at: indexPath)
        cell?.updateInfo(title: text, shouldSelect: contains.0)
        return cell ?? UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        headerView = KPITableHeaderView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.width, height: 40))
        if viewModel.accountType == .accounts {
            headerView?.configure(title: "ACCOUNTS (MAX \(viewModel.selectedAccounts.count)/5)")
        } else {
            headerView?.configure(title: "STORES (MAX \(viewModel.selectedStores.count)/5)")
        }
        return headerView
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let (contains, index) = viewModel.checkRecordShouldSelect(at: indexPath)
        if contains {
            viewModel.didSelectRecord(at: indexPath, removedIndex: index)
        } else {
            if !((viewModel.selectedAccounts.count > 4) || (viewModel.selectedStores.count > 4)) {
                viewModel.didSelectRecord(at: indexPath)
            }
        }
        updateSelectedCount()
        tableView.reloadRows(at: [indexPath], with: .automatic)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
}
