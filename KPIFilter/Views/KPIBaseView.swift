//
//  KPIBaseView.swift
//  KPIFilter
//
//  Created by Sravan kumar on 8/1/20.
//  Copyright © 2020 Prakash Maripalli. All rights reserved.
//

import UIKit

class KPIBaseView: UIView {
    lazy var tableView: UITableView = {
        let view = UITableView(frame: .zero, style: .plain)
        view.backgroundColor = UIColor.viewBackground
        view.translatesAutoresizingMaskIntoConstraints = false
        view.separatorInset = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let nibView = nib.instantiate(withOwner: self, options: nil).first as! UIView
        return nibView
    }
    
    func configure() {
        tableView.tableFooterView = UIView(frame: .zero)
        addSubview(tableView)
        tableView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 0).isActive = true
        tableView.topAnchor.constraint(equalTo: topAnchor, constant: 0).isActive = true
        trailingAnchor.constraint(equalTo: tableView.trailingAnchor, constant: 0).isActive = true
        bottomAnchor.constraint(equalTo: tableView.bottomAnchor, constant: 0).isActive = true
    }
}
