//
//  KPITableHeaderView.swift
//  KPIFilter
//
//  Created by Sravan kumar on 8/2/20.
//  Copyright © 2020 Prakash Maripalli. All rights reserved.
//

import UIKit

class KPITableHeaderView: KPIBaseView {

    @IBOutlet weak var titleLbl: UILabel!
    
    private var view: UIView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setup() {
        view = loadViewFromNib()
        
        view.translatesAutoresizingMaskIntoConstraints = false
        addSubview(view)
        view.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 0).isActive = true
        trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0).isActive = true
        bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0).isActive = true
        view.topAnchor.constraint(equalTo: topAnchor, constant: 0).isActive = true
        
        view.backgroundColor = UIColor.viewBackground
        titleLbl.textColor = .darkGray
        titleLbl.font = UIFont.systemFont(ofSize: 15)
    }
    
    func configure(title: String) {
        titleLbl.text = title
    }
}
