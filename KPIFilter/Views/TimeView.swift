//
//  TimeView.swift
//  KPIFilter
//
//  Created by Sravan kumar on 8/4/20.
//  Copyright © 2020 Prakash Maripalli. All rights reserved.
//

import UIKit

class TimeView: KPIBaseView {
    
    var viewModel: TimeVM!
    private var view: UIView!
    private var selectedIndex: Int = -1
    
    @IBOutlet weak var segment: UISegmentedControl!
    
    init(viewModel: TimeVM) {
        super.init(frame: .zero)
        self.viewModel = viewModel
        setup()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setup() {
        view = loadViewFromNib()
        view.translatesAutoresizingMaskIntoConstraints = false
        addSubview(view)
        
        view.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 0).isActive = true
        view.topAnchor.constraint(equalTo: topAnchor, constant: 0).isActive = true
        trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0).isActive = true
        bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0).isActive = true
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView(frame: .zero)
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "Cell")

        view.addSubview(tableView)
        tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0).isActive = true
        tableView.topAnchor.constraint(equalTo: segment.bottomAnchor, constant: 20).isActive = true
        view.trailingAnchor.constraint(equalTo: tableView.trailingAnchor, constant: 0).isActive = true
        view.bottomAnchor.constraint(equalTo: tableView.bottomAnchor, constant: 0).isActive = true
        
        customize()
    }
    
    func customize() {
        segment.selectedSegmentIndex = viewModel.type.getIndex()
        selectedIndex = viewModel.fetchFiscalDataIndex()
    }
    
    @IBAction func segmentTapped(_ sender: UISegmentedControl) {
        selectedIndex = -1
        if sender.selectedSegmentIndex == 0 {
            viewModel.type = .week
        } else if sender.selectedSegmentIndex == 1 {
            viewModel.type = .quarter
        } else if sender.selectedSegmentIndex == 2 {
            viewModel.type = .year
        }
        tableView.reloadData()
    }
}

extension TimeView: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRows()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell")
        cell?.textLabel?.text = viewModel.dataSource(at: indexPath)?.name
        if selectedIndex == indexPath.row {
            cell?.accessoryType = .checkmark
        } else {
            cell?.accessoryType = .none
        }
        return cell ?? UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel.selectedFiscalData = viewModel.dataSource(at: indexPath)
        viewModel.selectedFiscalType = viewModel.type
        if selectedIndex != indexPath.row {
            let cell = tableView.cellForRow(at: IndexPath(row: selectedIndex, section: 0))
            cell?.accessoryType = .none
        }
        selectedIndex = indexPath.row
        tableView.reloadRows(at: [indexPath], with: .automatic)
    }
}
