//
//  TerritoryVM.swift
//  KPIFilter
//
//  Created by Sravan kumar on 8/2/20.
//  Copyright © 2020 Prakash Maripalli. All rights reserved.
//

import Foundation

class TerritoryVM: BaseViewModel {
    struct TerritoryModel: Hashable {
        let id: Int?
        let name: String?
    }
    
    var territory: Territory?
    var selectedLevel: Int = 0
    var selectedTerritory: String?
    var results: [TerritoryModel] = [TerritoryModel]()
    
    init(level: Int = 0, name: String? = nil) {
        super.init()
        self.selectedLevel = level
        selectedTerritory = name
        loadData()
    }
    
    private func loadData() {
        territory = loadJson(resource: "terittory_data", model: Territory.self)
    }
    
    func numberOfRows() -> Int {
        if selectedLevel == 0 {
            return 7
        } else {
            levelsData(at: selectedLevel)
            return results.count
        }
    }
    
    func dataSource(at indexPath: IndexPath) -> String {
        return "Level \(indexPath.row + 1)"
    }
    
    func levelsData(at level: Int) {
        let id = "d_territory_level_\(level)_id_chsi"
        guard var filterRecords = territory?.result else { return }
        if let name = selectedTerritory {
            filterRecords = filterRecords.filter { (record) -> Bool in
                if level == 2 { return record.d_territory_level_1_name_chsi == name }
                if level == 3 { return record.d_territory_level_2_name_chsi == name }
                if level == 4 { return record.d_territory_level_3_name_chsi == name }
                if level == 5 { return record.d_territory_level_4_name_chsi == name }
                if level == 6 { return record.d_territory_level_5_name_chsi == name }
                if level == 7 { return record.d_territory_level_6_name_chsi == name }
                if level == 8 { return record.d_territory_level_7_name_chsi == name }
                return true
            }
        }
        var temp = [TerritoryModel]()
        filterRecords.forEach { (record) in
            let item = getRecord(record: record, level: id)
            if let item = item {
                temp.append(item)
            }
        }
        results = temp.unique()
        results.insert(TerritoryModel(id: level, name: "All"), at: 0)
    }
    
    func getRecord(record: Territory.TerritoryRecord , level: String) -> TerritoryModel? {
        switch level {
        case "d_territory_level_1_id_chsi":
            return TerritoryModel(id: record.d_territory_level_1_id_chsi, name: record.d_territory_level_1_name_chsi)
        case "d_territory_level_2_id_chsi":
            return TerritoryModel(id: record.d_territory_level_2_id_chsi, name: record.d_territory_level_2_name_chsi)
        case "d_territory_level_3_id_chsi":
            return TerritoryModel(id: record.d_territory_level_3_id_chsi, name: record.d_territory_level_3_name_chsi)
        case "d_territory_level_4_id_chsi":
            return TerritoryModel(id: record.d_territory_level_4_id_chsi, name: record.d_territory_level_4_name_chsi)
        case "d_territory_level_5_id_chsi":
            return TerritoryModel(id: record.d_territory_level_5_id_chsi, name: record.d_territory_level_5_name_chsi)
        case "d_territory_level_6_id_chsi":
            return TerritoryModel(id: record.d_territory_level_6_id_chsi, name: record.d_territory_level_6_name_chsi)
        case "d_territory_level_7_id_chsi":
            return TerritoryModel(id: record.d_territory_level_7_id_chsi, name: record.d_territory_level_7_name_chsi)
        default:
            break
        }
        return nil
    }
}

extension Sequence where Iterator.Element: Hashable {
    func unique() -> [Iterator.Element] {
        var seen: Set<Iterator.Element> = []
        return filter { seen.insert($0).inserted }
    }
}

extension Array where Element: Hashable {
  func uniqueElements() -> [Element] {
    var seen = Set<Element>()
    var out = [Element]()

    for element in self {
      if !seen.contains(element) {
        out.append(element)
        seen.insert(element)
      }
    }

    return out
  }
}
