//
//  AccountsVM.swift
//  KPIFilter
//
//  Created by Sravan kumar on 8/1/20.
//  Copyright © 2020 Prakash Maripalli. All rights reserved.
//

import Foundation

class AccountsVM: BaseViewModel {
    enum AccountsType {
        case accounts
        case stores
    }
    var accountType: AccountsType!
    var accounts: [String]?
    var accountsJson: Accounts?
    var storeJson: Stores?
    var stores: [String]?
    var selectedAccounts: [Accounts.AccountRecord]!
    var selectedStores: [Stores.StoreRecord]!

    init(with type: AccountsType,
         accounts: [Accounts.AccountRecord]? = nil,
         stores: [Stores.StoreRecord]? = nil) {
        super.init()
        self.accountType = type
        self.selectedAccounts = accounts ?? [Accounts.AccountRecord]()
        self.selectedStores = stores ?? [Stores.StoreRecord]()
        loadData()
    }
    
    private func loadData() {
        if accountType == .accounts {
            accountsJson = loadJson(resource: "Accounts", model: Accounts.self)
            guard let temp = accountsJson?.result else { return }
            accounts = removeAccountsDuplicates(results: temp)
        } else {
            storeJson = loadJson(resource: "Stores", model: Stores.self)
            guard let temp = storeJson?.result else { return }
            stores = removeStoreDuplicates(results: temp)
        }
    }
    
    private func removeAccountsDuplicates(results: [Accounts.AccountRecord]) -> [String] {
        var temp = [String]()
        results.forEach { (record) in
            if let name = record.d_loc_account_name_chsi, let id = record.d_hq_apple_id_chsi {
                let str =  name + " " + "\(id)"
                if !temp.contains(str) {
                    temp.append(str)
                }
            }
        }
        return temp
    }
    
    private func removeStoreDuplicates(results: [Stores.StoreRecord]) -> [String] {
        var temp = [String]()
        results.forEach { (record) in
            if let name = record.d_loc_store_name_chsi, let id = record.d_apple_id_chsi {
                let str =  name + " " + "\(id)"
                if !temp.contains(str) {
                    temp.append(str)
                }
            }
        }
        return temp
    }
    
    private func getSelectedAccount(name: String) -> Accounts.AccountRecord? {
        guard let temp = accountsJson?.result else { return nil }
        let result = temp.filter { (record) -> Bool in
            if let name1 = record.d_loc_account_name_chsi, let id = record.d_hq_apple_id_chsi {
                let str =  name1 + " " + "\(id)"
                return str == name
            }
            return false
        }
        return result.first
    }
    
    private func getSelectedStore(name: String) -> Stores.StoreRecord? {
           guard let temp = storeJson?.result else { return nil }
           let result = temp.filter { (record) -> Bool in
               if let name1 = record.d_loc_store_name_chsi, let id = record.d_apple_id_chsi {
                   let str =  name1 + " " + "\(id)"
                   return str == name
               }
               return false
           }
           return result.first
       }
    
    func fetchNavigationTitle() -> String {
        if accountType == .accounts { return "Accounts"}
        return "Stores"
    }
    
    func fetchNumberOfRows() -> Int {
        if accountType == .accounts {
            guard let accounts = accounts else { return 0 }
            return accounts.count
        } else {
            guard let stores = stores else { return 0 }
            return stores.count
        }
    }
    
    func fetchDataSource(at indexPath: IndexPath) -> String? {
        if accountType == .accounts {
            guard let accounts = accounts else { return nil }
            let record = accounts[indexPath.row]
            return record
        } else {
            guard let stores = stores else { return nil }
            let record = stores[indexPath.row]
            return record
        }
    }
    
    func didSelectRecord(at indexPath: IndexPath, removedIndex: Int? = nil) {
        if accountType == .accounts {
            guard let accounts = accounts else { return }
            guard let index = removedIndex else {
                if let record = getSelectedAccount(name: accounts[indexPath.row]) {
                    selectedAccounts.append(record)
                }
                return
            }
            selectedAccounts.remove(at: index)
            
        } else {
            guard let stores = stores else { return }
            guard let index = removedIndex else {
                if let record = getSelectedStore(name: stores[indexPath.row]) {
                    selectedStores.append(record)
                }
                return
            }
            selectedStores.remove(at: index)
        }
    }
    
    func checkRecordShouldSelect(at indexPath: IndexPath) -> (Bool, Int) {
        let name = fetchDataSource(at: indexPath)
        if accountType == .accounts {
            for (index, account) in selectedAccounts.enumerated() {
                if let temp = account.d_loc_account_name_chsi, let id = account.d_hq_apple_id_chsi {
                    let name1 = temp + " " + "\(id)"
                    if name1 == name {
                        return (true, index)
                    }
                }
            }
        } else {
            for (index, store) in selectedStores.enumerated() {
                if let temp = store.d_loc_store_name_chsi, let id = store.d_apple_id_chsi {
                    let name1 = temp + " " + "\(id)"
                    if name1 == name {
                        return (true, index)
                    }
                }
            }
        }
        return (false, 0)
    }
}
