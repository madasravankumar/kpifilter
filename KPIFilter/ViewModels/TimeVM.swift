//
//  TimeVM.swift
//  KPIFilter
//
//  Created by Sravan kumar on 8/4/20.
//  Copyright © 2020 Prakash Maripalli. All rights reserved.
//

import Foundation

class TimeVM: BaseViewModel {
    enum SelectedType {
        case week
        case quarter
        case year
        
        func getIndex() -> Int {
            if self == .week { return 0 }
            if self == .quarter { return 1 }
            if self == .year { return 2 }
            return 0
        }
    }
    
    struct TimeModel {
        let name: String
        let id: String
    }
    
    var fiscal: Fiscal?
    var type: SelectedType = .year
    var years: [String] = [String]()
    var quarters: [String] = [String]()
    var weeks: [String] = [String]()
    var selectedFiscalData: TimeModel?
    var selectedFiscalType: SelectedType?
    
    init(type: SelectedType, fiscalData: TimeModel? = nil) {
        super.init()
        self.type = type
        self.selectedFiscalType = type
        self.selectedFiscalData = fiscalData
        loadData()
    }
    
    private func loadData() {
        fiscal = loadJson(resource: "FiscalData", model: Fiscal.self)
        guard let results = fiscal?.result else { return }
        createDataSource(results: results)
    }
    
    private func createDataSource(results: [Fiscal.FiscalRecord]) {
        results.forEach { (res) in
            if let year = res.d_rolling_year {
                years.append(year)
            }
            if let name = res.d_rolling_quarter {
                quarters.append(name)
            }
            if let name = res.d_rolling_week {
                weeks.append(name)
            }
        }
        years = sortedArray(res: years)
        quarters = sortedArray(res: quarters)
        weeks = sortedArray(res: weeks)
    }
    
    private func sortedArray(res: [String]) -> [String] {
        return res.unique().sorted(by: { (obj1, obj2) -> Bool in
            if let role1 = Int(obj1), let role2 = Int(obj2) {
                return role1 > role2
            }
            return true
        })
    }
    
    func fetchFiscalDataIndex() -> Int {
        if selectedFiscalType == type {
            guard let model = selectedFiscalData else { return -1 }
            if selectedFiscalType == .year {
                return getIndex(result: years, at: model)
            } else if selectedFiscalType == .quarter {
                return getIndex(result: quarters, at: model)
            } else if selectedFiscalType == .week {
                return getIndex(result: weeks, at: model)
            }
        }
        return -1
    }
    
    private func getIndex(result: [String], at model: TimeModel) -> Int {
        for (index, item) in result.enumerated() {
            if model.id == item {
                return index
            }
        }
        return -1
    }
    
    func numberOfRows() -> Int {
        if type == .year {
            return years.count
        } else if type == .quarter {
            return quarters.count
        } else if type == .week {
            return weeks.count
        }
        return 0
    }
    
    func dataSource(at indexPath: IndexPath) -> TimeModel? {
        if type == .year {
            return filterYears(at: indexPath)
            
        } else if type == .quarter {
            return filterQuaters(at: indexPath)
        } else if type == .week {
            return filterWeeks(at: indexPath)
        }
        return nil
    }
    
    private func filterYears(at indexPath: IndexPath) -> TimeModel? {
        guard let results = fiscal?.result else { return nil }
        let value = years[indexPath.row]
        let temp = results.filter { (obj1) -> Bool in
            if let year =  obj1.d_rolling_year,  value == year {
                return true
            }
            return false
        }
        if let resultObj = temp.first {
            let name = "\(resultObj.d_fiscal_year ?? 2020)"
            let id = resultObj.d_rolling_quarter ?? ""
            return TimeModel(name: name, id: id)
        }
        return nil
    }
    
    private func filterQuaters(at indexPath: IndexPath) -> TimeModel? {
        guard let results = fiscal?.result else { return nil }
        let value = quarters[indexPath.row]
        let temp = results.filter { (obj1) -> Bool in
            if let quarter =  obj1.d_rolling_quarter,  value == quarter {
                return true
            }
            return false
        }
        if let resultObj = temp.first {
            let name = resultObj.d_fiscal_yr_qtr_label ?? ""
            let id = resultObj.d_rolling_quarter ?? ""
            return TimeModel(name: name, id: id)
        }
        return nil
    }
    
    private func filterWeeks(at indexPath: IndexPath) -> TimeModel? {
        guard let results = fiscal?.result else { return nil }
        let value = weeks[indexPath.row]
        let temp = results.filter { (obj1) -> Bool in
            if let week =  obj1.d_rolling_week,  value == week {
                return true
            }
            return false
        }
        if let resultObj = temp.first {
            let name = resultObj.d_cycw_label ?? ""
            let id = resultObj.d_rolling_week ?? ""
            return TimeModel(name: name, id: id)
        }
        return nil
    }
}
