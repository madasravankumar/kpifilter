//
//  FilterBaseVM.swift
//  KPIFilter
//
//  Created by Sravan kumar on 8/1/20.
//  Copyright © 2020 Prakash Maripalli. All rights reserved.
//

import Foundation

class BaseViewModel {
    init() { }
    
    func loadJson<T: Codable>(resource: String, model: T.Type) -> T? {
        if let path = Bundle.main.path(forResource: resource, ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .alwaysMapped)
                let decoder = JSONDecoder()
                return try? decoder.decode(T.self, from: data)
            } catch {
                // handle error
                print(error)
            }
        }
        return nil
    }
    
    func loadText(resource: String) -> [String: Any]? {
        if let path = Bundle.main.path(forResource: resource, ofType: "txt") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let json = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
                return json as? [String: Any]
            } catch { }
        }
        return nil
    }
}

class FilterBaseVM: BaseViewModel {
    var fiscalData: TimeVM.TimeModel?
    var fiscalType: TimeVM.SelectedType?
    var territory: String?
    var territoryLevel: Int?
    var accounts: [Accounts.AccountRecord]?
    var stores: [Stores.StoreRecord]?
    
    func fetchHeaderTitle(at section: Int) -> String {
        switch section {
        case 0:
            return "TIME"
        case 1:
            return "TERRITORY"
        case 2:
            return "ACCOUNTS (ALL OR MAX 30)"
        case 3:
            return "STORES (ALL OR MAX 30)"
        default:
            return ""
        }
    }
}
