//
//  Extensions.swift
//  KPIFilter
//
//  Created by Sravan kumar on 8/29/20.
//  Copyright © 2020 Prakash Maripalli. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    static var  viewBackground: UIColor? {
        return UIColor(named: "viewbackground")
    }
    
    static var  cellBackground: UIColor? {
        return UIColor(named: "cellbackground")
    }
}

extension Notification.Name {
    static let NSNotificationWillSelectTerritory = Notification.Name("NSNotificationWillSelectTerritory")
}
