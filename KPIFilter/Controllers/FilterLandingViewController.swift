//
//  FilterLandingViewController.swift
//  KPIFilter
//
//  Created by Prakash Maripalli on 7/30/20.
//  Copyright © 2020 Prakash Maripalli. All rights reserved.
//

import UIKit

class FilterLandingViewController: UIViewController, FilterLandingProtocol, AccountsDataSourceProtocol, FiscalDataSourceProtocol {
    
    var viewModel: FilterBaseVM!
    var rootView: FilterLandingView {
        return self.view as! FilterLandingView
    }
    
    init(viewModel: FilterBaseVM) {
        super.init(nibName: nil, bundle: nil)
        self.viewModel = viewModel
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        super.loadView()
        let view = FilterLandingView(viewModel: self.viewModel)
        view.delegate = self
         self.view = view
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configure()
    }
    
    // MARK:
    private func configure() {
        navigationItem.title = "Filter"
        let cancel = UIBarButtonItem(title: "Cancel", style: .done, target: self, action: #selector(didTapCancel))
        let apply = UIBarButtonItem(title: "Apply", style: .done, target: self, action: #selector(didTapOnApply))
        navigationItem.leftBarButtonItem = cancel
        navigationItem.rightBarButtonItem = apply
        NotificationCenter.default.addObserver(self, selector: #selector(selectedTerritory(sender:)), name: .NSNotificationWillSelectTerritory, object: nil)
    }
    
    @objc func selectedTerritory(sender: Notification) {
        if let userInfo = sender.userInfo {
            viewModel.territoryLevel = userInfo["level"] as? Int
            viewModel.territory = userInfo["territory"] as? String
            rootView.updateTerritoryLevel()
        }
    }
    
    func didTap(at index: Int) {
        switch index {
        case 0:
            showQuarterView()
        case 1:
            showTerritory()
        case 2:
            showAccounts(type: .accounts)
        case 3:
            showAccounts(type: .stores)
        default:
            break
        }
    }
    
    func didTapTerritoryLevel(level: Int) {
        let vm = TerritoryVM(level: level, name: nil)
        let vc = TerritoryViewController(viewModel: vm)
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func didTapCancel() {
        dismiss(animated: true, completion: nil)
        print("Screen Dismissed")
    }
     
    @objc func didTapOnApply() {
        print("Filters Applied")
    }
    
    private func showQuarterView() {
        let model = TimeVM(type: viewModel.fiscalType ?? .year, fiscalData: viewModel.fiscalData)
        let vc = TimeViewController(viewModel: model)
        vc.delegate = self
        navigationController?.pushViewController(vc, animated: true)
    }
    
    private func showTerritory() {
        let model = TerritoryVM()
        let vc = TerritoryViewController(viewModel: model)
        navigationController?.pushViewController(vc, animated: true)
    }
    
    private func showAccounts(type: AccountsVM.AccountsType) {
        let model = AccountsVM(with: type, accounts: viewModel.accounts, stores: viewModel.stores)
        let vc = AccountsViewController(viewModel: model)
        vc.delegate = self
        navigationController?.pushViewController(vc, animated: true)
    }
    
    // MARK: AccountsDataSourceProtocol
    func selectedRecords(accounts: [Accounts.AccountRecord], stores: [Stores.StoreRecord]) {
        if !accounts.isEmpty {
            viewModel.accounts = accounts
        } else {
            viewModel.stores = stores
        }
    }
    
    func selectedFiscalData(time: TimeVM.TimeModel?, type: TimeVM.SelectedType?) {
        viewModel.fiscalData = time
        viewModel.fiscalType = type
        rootView.updateFiscalData()
    }
}
