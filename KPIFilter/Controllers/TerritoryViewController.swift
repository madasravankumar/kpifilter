//
//  TerritoryViewController.swift
//  KPIFilter
//
//  Created by Sravan kumar on 8/2/20.
//  Copyright © 2020 Prakash Maripalli. All rights reserved.
//

import UIKit

class TerritoryViewController: UIViewController, TerritoryViewDelegate {

    var viewModel: TerritoryVM!

    init(viewModel: TerritoryVM) {
        super.init(nibName: nil, bundle: nil)
        self.viewModel = viewModel
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        super.loadView()
        let view = TerritoryView(viewModel: viewModel)
        view.delegate = self
        self.view = view
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        configure()
    }
    
    // MARK:
    private func configure() {
        navigationItem.title = "Territory"
    }
    
    func didSelect(level: Int, territory: String?) {
        let vm = TerritoryVM(level: level, name: territory)
        let vc = TerritoryViewController(viewModel: vm)
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func didTapOnRadioBtn() {
        navigationController?.popToRootViewController(animated: true)
    }
}
