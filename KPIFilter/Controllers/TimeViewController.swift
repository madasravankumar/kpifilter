//
//  TimeViewController.swift
//  KPIFilter
//
//  Created by Sravan kumar on 8/4/20.
//  Copyright © 2020 Prakash Maripalli. All rights reserved.
//

import UIKit
protocol FiscalDataSourceProtocol: NSObjectProtocol {
    func selectedFiscalData(time: TimeVM.TimeModel?, type: TimeVM.SelectedType?)
}

class TimeViewController: UIViewController {
    var viewModel: TimeVM!
    weak var delegate: FiscalDataSourceProtocol?

    init(viewModel: TimeVM) {
        super.init(nibName: nil, bundle: nil)
        self.viewModel = viewModel
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        super.loadView()
        let view = TimeView(viewModel: viewModel)
        self.view = view
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        delegate?.selectedFiscalData(time: viewModel.selectedFiscalData, type: viewModel.selectedFiscalType)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
