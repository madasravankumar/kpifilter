//
//  AccountsViewController.swift
//  KPIFilter
//
//  Created by Sravan kumar on 8/1/20.
//  Copyright © 2020 Prakash Maripalli. All rights reserved.
//

import Foundation
import UIKit

protocol AccountsDataSourceProtocol: NSObjectProtocol {
    func selectedRecords(accounts: [Accounts.AccountRecord], stores: [Stores.StoreRecord])
}

class AccountsViewController: UIViewController {
    weak var delegate: AccountsDataSourceProtocol?
    
    var viewModel: AccountsVM!
    
    init(viewModel: AccountsVM) {
        super.init(nibName: nil, bundle: nil)
        self.viewModel = viewModel
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        super.loadView()
        self.view = AccountsView(viewModel: viewModel)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configure()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        delegate?.selectedRecords(accounts: viewModel.selectedAccounts, stores: viewModel.selectedStores)
    }
    
    // MARK:
    private func configure() {
        navigationItem.title = viewModel.fetchNavigationTitle()
    }
}
