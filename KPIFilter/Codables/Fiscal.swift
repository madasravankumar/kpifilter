//
//  Fiscal.swift
//  KPIFilter
//
//  Created by Sravan kumar on 8/1/20.
//  Copyright © 2020 Prakash Maripalli. All rights reserved.
//

import Foundation

class Fiscal: KPICodable {
    struct FiscalRecord: Codable, Hashable {
        var d_cycw_label: String?
        var d_fiscal_yr_qtr_label: String?
        var d_fiscal_year: Int?
        var d_rolling_year: String?
        var d_rolling_quarter: String?
        var d_rolling_week: String?
        
        enum CodingKeys: String, CodingKey {
            case d_cycw_label
            case d_fiscal_yr_qtr_label
            case d_fiscal_year
            case d_rolling_year
            case d_rolling_quarter
            case d_rolling_week
        }
        
        init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            d_cycw_label = try? values.decode(String.self, forKey: .d_cycw_label)
            d_fiscal_yr_qtr_label = try? values.decode(String.self, forKey: .d_fiscal_yr_qtr_label)
            d_rolling_year = try? values.decode(String.self, forKey: .d_rolling_year)
            d_rolling_quarter = try? values.decode(String.self, forKey: .d_rolling_quarter)
            d_rolling_week = try? values.decode(String.self, forKey: .d_rolling_week)
            d_fiscal_year = try? values.decode(Int.self, forKey: .d_fiscal_year)
        }
        
    }
        
    enum CodingKeys: String, CodingKey {
        case result
    }
    var result: [FiscalRecord]?
    
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        result = try? values.decodeIfPresent([FiscalRecord].self, forKey: .result)
        try super.init(from: decoder)
    }
}
