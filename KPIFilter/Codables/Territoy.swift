//
//  Territoy.swift
//  KPIFilter
//
//  Created by Sravan kumar on 8/2/20.
//  Copyright © 2020 Prakash Maripalli. All rights reserved.
//

import Foundation

class Territory: KPICodable {
    class TerritoryRecord: Codable {
        var d_territory_level_1_id_chsi: Int?
        var d_territory_level_2_id_chsi: Int?
        var d_territory_level_3_id_chsi: Int?
        var d_territory_level_4_id_chsi: Int?
        var d_territory_level_5_id_chsi: Int?
        var d_territory_level_6_id_chsi: Int?
        var d_territory_level_7_id_chsi: Int?
        var d_territory_level_1_name_chsi: String?
        var d_territory_level_2_name_chsi: String?
        var d_territory_level_3_name_chsi: String?
        var d_territory_level_4_name_chsi: String?
        var d_territory_level_5_name_chsi: String?
        var d_territory_level_6_name_chsi: String?
        var d_territory_level_7_name_chsi: String?
        
        enum CodingKeys: String, CodingKey {
            case d_territory_level_1_id_chsi
            case d_territory_level_2_id_chsi
            case d_territory_level_3_id_chsi
            case d_territory_level_4_id_chsi
            case d_territory_level_5_id_chsi
            case d_territory_level_6_id_chsi
            case d_territory_level_7_id_chsi
            case d_territory_level_1_name_chsi
            case d_territory_level_2_name_chsi
            case d_territory_level_3_name_chsi
            case d_territory_level_4_name_chsi
            case d_territory_level_5_name_chsi
            case d_territory_level_6_name_chsi
            case d_territory_level_7_name_chsi
        }
        
        required init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            d_territory_level_1_id_chsi = try? values.decode(Int.self, forKey: .d_territory_level_1_id_chsi)
            d_territory_level_2_id_chsi = try? values.decode(Int.self, forKey: .d_territory_level_2_id_chsi)
            d_territory_level_3_id_chsi = try? values.decode(Int.self, forKey: .d_territory_level_3_id_chsi)
            d_territory_level_4_id_chsi = try? values.decode(Int.self, forKey: .d_territory_level_4_id_chsi)
            d_territory_level_5_id_chsi = try? values.decode(Int.self, forKey: .d_territory_level_5_id_chsi)
            d_territory_level_6_id_chsi = try? values.decode(Int.self, forKey: .d_territory_level_6_id_chsi)
            d_territory_level_7_id_chsi = try? values.decode(Int.self, forKey: .d_territory_level_7_id_chsi)
            d_territory_level_1_name_chsi = try? values.decode(String.self, forKey: .d_territory_level_1_name_chsi)
            d_territory_level_2_name_chsi = try? values.decode(String.self, forKey: .d_territory_level_2_name_chsi)
            d_territory_level_3_name_chsi = try? values.decode(String.self, forKey: .d_territory_level_3_name_chsi)
            d_territory_level_4_name_chsi = try? values.decode(String.self, forKey: .d_territory_level_4_name_chsi)
            d_territory_level_5_name_chsi = try? values.decode(String.self, forKey: .d_territory_level_5_name_chsi)
            d_territory_level_6_name_chsi = try? values.decode(String.self, forKey: .d_territory_level_6_name_chsi)
            d_territory_level_7_name_chsi = try? values.decode(String.self, forKey: .d_territory_level_7_name_chsi)
        }
    }
    
    var result: [TerritoryRecord]?
    enum CodingKeys: String, CodingKey {
        case result
    }
    
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        result = try? values.decodeIfPresent([TerritoryRecord].self, forKey: .result)
        try super.init(from: decoder)
    }
}
