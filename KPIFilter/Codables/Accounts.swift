//
//  Accounts.swift
//  KPIFilter
//
//  Created by Sravan kumar on 8/1/20.
//  Copyright © 2020 Prakash Maripalli. All rights reserved.
//

import Foundation

class KPICodable: Codable {
    let status: String
    let requestId: String
}

class Accounts: KPICodable {
    struct AccountRecord: Codable, Hashable {
        var d_loc_account_name_chsi: String?
        var d_derived_hq_ind_chsi: String?
        var d_hq_apple_id_chsi: Double?
        
        enum CodingKeys: String, CodingKey {
            case d_loc_account_name_chsi
            case d_derived_hq_ind_chsi
            case d_hq_apple_id_chsi
        }
        init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            d_loc_account_name_chsi = try? values.decodeIfPresent(String.self, forKey: .d_loc_account_name_chsi)
            d_derived_hq_ind_chsi = try? values.decodeIfPresent(String.self, forKey: .d_derived_hq_ind_chsi)
            d_hq_apple_id_chsi = try? values.decodeIfPresent(Double.self, forKey: .d_hq_apple_id_chsi)
        }
    }
    
    var result: [AccountRecord]?
    enum CodingKeys: String, CodingKey {
        case result
    }
    
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        result = try? values.decodeIfPresent([AccountRecord].self, forKey: .result)
        try super.init(from: decoder)
    }
}




