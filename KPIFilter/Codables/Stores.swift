//
//  Stores.swift
//  KPIFilter
//
//  Created by Sravan kumar on 8/1/20.
//  Copyright © 2020 Prakash Maripalli. All rights reserved.
//

import Foundation

class Stores: KPICodable {
    struct StoreRecord: Codable, Hashable {
        var d_loc_store_name_chsi: String?
        var d_address_desc_chsi: String?
        var d_apple_id_chsi: Double?
        
        enum CodingKeys: String, CodingKey {
            case d_loc_store_name_chsi
            case d_address_desc_chsi
            case d_apple_id_chsi
        }
        
        init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            d_loc_store_name_chsi = try? values.decodeIfPresent(String.self, forKey: .d_loc_store_name_chsi)
            d_address_desc_chsi = try? values.decodeIfPresent(String.self, forKey: .d_address_desc_chsi)
            d_apple_id_chsi = try? values.decodeIfPresent(Double.self, forKey: .d_apple_id_chsi)
        }
    }
    enum CodingKeys: String, CodingKey {
        case result
    }
    var result: [StoreRecord]?
    
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        result = try? values.decodeIfPresent([StoreRecord].self, forKey: .result)
        try super.init(from: decoder)
    }
}
